import React from 'react';
import TopNav from './Layout/TopNav';
import Head from './Layout/Head';

function News(){
    return(

        <div>
            <Head/>
            <TopNav/>
        </div>
    )
}

export default News;