import React from 'react';
import '../Layout/Body.css';

const CovidInformation = ({information}) => {
    const refs = information.reduce((acc,value)=>{
        acc[value.title] = React.createRef();
        return acc;
    },{});

    const handleClick = title =>
    refs[title].current.scrollIntoView({
        behaviour: 'smooth',
        block: 'start',
    });

    return(
        <div>
            <div className="sidenav">
                {information.map(info=>{
                    return(
                        <div key={info.title}>
                            <a onClick={() => handleClick(info.title)}>{info.title}</a>
                        </div>
                    );
                })}
            </div>
            <div className="content">
                {information.map(info=>{
                   return(
                       <div key={info.title} ref={refs[info.title]}>
                          <h2>{info.title}</h2>
                          <br/>
                          <h3>{info.subTitle}</h3>
                          <h4>{info.contentRow1}</h4>
                          <h4>{info.contentRow2}</h4>
                          <h4>{info.contentRow3}</h4>
                          <h4>{info.contentRow4}</h4>
                          <h4>{info.contentRow5}</h4>
                          <h4>{info.contentRow6}</h4>
                          <h4>{info.subTitle}</h4>
                          <br/><br/><br/>
                        </div>
                        
                    );
            
                })}
            </div>
        </div>
    )
}

export default CovidInformation;    