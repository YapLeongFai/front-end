import React, {Component} from 'react';
import Head from '../Layout/Head';
import TopNav from '../Layout/TopNav';
import '../Layout/Body.css';
import CovidInformation from './CovidInformation';

class Information extends Component {

  state={
    information:[]
  }

  componentDidMount(){
    let Token="Bearer " + localStorage.getItem("token")
    fetch('/covid19/information',{
      method: 'GET',
      headers:{
        'Authorization': Token,
        'Content-Type': 'application/json'
      }
    })
    .then(response => response.json())
    .then(response => {
      console.log(response)
      this.setState({information:response})
    })
    .catch(console.log)
  }

  render(){
     return(
     <div>
       <div>
         <Head/>
         <TopNav/>
       </div>

       <div className="Body"> 
         <CovidInformation information={this.state.information}/>
        </div>
      </div>
    );
  }
  
}

export default Information;
