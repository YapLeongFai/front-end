import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import '../Layout/Body.css';
import Head from '../Layout/Head';
import TopNav from '../Layout/TopNav';

export default class Login extends Component{
    constructor(props){
        super(props);
        const token = localStorage.getItem("token")

        let loggedIn = true
        if(token == null){
            loggedIn = false
        }
        else if(token.length <10){
            loggedIn = false
        }
        
        this.state={
            username: '',
            password: '',
            loggedIn
        }
    }

    handleChange = event =>{
        this.setState({[event.target.name]:event.target.value})
    }

    handleSubmit = event=>{
        event.preventDefault();
        console.log("username: " + this.state.name)
        console.log("password: " + this.state.password)
        const url = "/authenticate"
        const data = {username:this.state.username, password:this.state.password}

        fetch(url, {
            method:'POST',
            body: JSON.stringify(data),
            headers:{'Content-Type': 'application/json'
        }})
        .then((res) => res.json()
        .then((value)=>{
            localStorage.setItem("token",value.token)
            this.setState({loggedIn:true})
        }))
    }

    render(){
        if(this.state.loggedIn){
            return <Redirect to="/admin/admin_information"/>
        }
        return(
            <div>
                <div>
                    <Head/>
                    <TopNav/>
                </div>

                <div className="loginPage">
                    <h1>Login</h1>
                    <form onSubmit={this.handleSubmit}>
                        <label name="username">Username:</label>
                        <input type="text" name="username" onChange={this.handleChange}/><br/><br/>
                        <label name="password">Password:</label>
                        <input type="password" name="password" onChange={this.handleChange}/><br/><br/>
                        <input type="submit" value="login"/>
                    </form>
                </div>
            </div>
        )
    }
    

} 
    
    
  