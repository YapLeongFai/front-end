import React, {Component} from 'react';
import {Link, Redirect} from 'react-router-dom';
import './Admin.css';


class AdminInformation extends Component {
    constructor(props) {
        super(props)
        const token = localStorage.getItem("token")
        let loggedIn = true
        if (token == null) {
            loggedIn = false
        } else if (token.length < 10) {
            loggedIn = false
        }

        this.state = {
            loggedIn,
            information:[]
        }
    }

    componentDidMount(){
        let Token="Bearer " + localStorage.getItem("token")
        fetch('/covid19/information',{
            method: 'GET',
            headers:{
                'Authorization': Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(response => {
                console.log(response)
                this.setState({information:response})
            })
            .catch(console.log)
    }

    render() {
        if (this.state.loggedIn === false) {
            return <Redirect to="/login"/>
        }

        return (
            <div>
                <div className="column side">
                    <div className="sideHeader"/>
                    <a><Link to="/admin/admin_information">Information</Link></a>
                    <a><Link to="/logout">Logout</Link></a>
                </div>
                <div className="column middle">
                    <h1 className="header">Admin</h1>
                    <div>
                        {this.state.information.map(info => (
                            <div className="column middle" key={info.keyword}>
                                <h2>{info.title} ({info.keyword})</h2>
                                <br/>
                                <h3>{info.subTitle}</h3>
                                <h4>{info.contentRow1}</h4>
                                <h4>{info.contentRow2}</h4>
                                <h4>{info.contentRow3}</h4>
                                <h4>{info.contentRow4}</h4>
                                <h4>{info.contentRow5}</h4>
                                <h4>{info.contentRow6}</h4>
                                <h4>{info.subTitle}</h4>
                                <br/><br/><br/>
                            </div>
                        ))}
                    </div>
                </div>
                <div className="column side">
                    <div className="sideHeader"/>
                    <a><Link to="/admin/admin_information/add">Add</Link></a>
                    <a><Link to="/admin/admin_information/delete">Delete</Link></a>
                </div>
            </div>

        )
    }
}

export default AdminInformation;