import React, {Component} from 'react';
import {Link, Redirect} from 'react-router-dom';
import './Admin.css';

class AddInformation extends Component {
    constructor(props) {
        super(props)
        const token = localStorage.getItem("token")
        let loggedIn = true
        if (token == null) {
            loggedIn = false
        } else if (token.length < 10) {
            loggedIn = false
        }

        this.state = {
            loggedIn,
            keyword:'',
            title:'',
            subTitle:'',
            contentRow1:'',
            contentRow2:'',
            contentRow3:'',
            contentRow4:'',
            contentRow5:'',
            contentRow6:''
        }
    }

    handleChange = event =>{
        this.setState({[event.target.name]:event.target.value})
    }

    handleSubmit = event=>{
        event.preventDefault();
        const data = {keyword:this.state.keyword, title:this.state.title, subTitle: this.state.subTitle, contentRow1: this.state.contentRow1,
            contentRow2: this.state.contentRow2, contentRow3: this.state.contentRow3, contentRow4: this.state.contentRow4, contentRow5: this.state.contentRow5, contentRow6: this.state.contentRow6}
        let Token="Bearer " + localStorage.getItem("token")

        fetch("/covid19/information", {
            method:'POST',
            body: JSON.stringify(data),
            headers:{
                'Authorization': Token,
                'Content-Type': 'application/json'
            }})
            .then((res) => res.json()
                .then((value)=>{
                    localStorage.setItem("token",value.token)
                    this.setState({loggedIn:true})
                }))
    }



    render() {
        if (this.state.loggedIn === false) {
            return <Redirect to="/login"/>
        }

        return (
            <div>
                <div className="column side">
                    <div className="sideHeader"/>
                    <a><Link to="/admin/admin_information">Information</Link></a>
                    <a><Link to="/logout">Logout</Link></a>
                </div>
                <div className="column middle">
                    <h1 className="header">Admin</h1>
                    <div className="column middle">
                        <h1>Add Information</h1>
                        <form onSubmit={this.handleSubmit}>
                            <label name="keyword">Keyword</label>
                            <input type="text" name="keyword" onChange={this.handleChange}/><br/><br/>
                            <label name="title">Title</label>
                            <input type="text" name="title" onChange={this.handleChange}/><br/><br/>
                            <label name="subTitle">Sub-Title</label>
                            <input type="text" name="subTitle" onChange={this.handleChange}/><br/><br/>
                            <label name="contentRow1">content Row1</label>
                            <input type="text" name="contentRow1" onChange={this.handleChange}/><br/><br/>
                            <label name="contentRow2">        Row2</label>
                            <input type="text" name="contentRow2" onChange={this.handleChange}/><br/><br/>
                            <label name="contentRow3">        Row3</label>
                            <input type="text" name="contentRow3" onChange={this.handleChange}/><br/><br/>
                            <label name="contentRow4">        Row4</label>
                            <input type="text" name="contentRow4" onChange={this.handleChange}/><br/><br/>
                            <label name="contentRow5">        Row5</label>
                            <input type="text" name="contentRow5" onChange={this.handleChange}/><br/><br/>
                            <label name="contentRow6">        Row6</label>
                            <input type="text" name="contentRow6" onChange={this.handleChange}/><br/><br/>
                            <input type="submit" value="Add"/>
                        </form>
                    </div>
                </div>
                <div className="column side">
                    <div className="sideHeader"/>
                    <a><Link to="/admin/admin_information/add">Add</Link></a>
                    <a><Link to="/admin/admin_information/delete">Delete</Link></a>
                </div>

            </div>

        )
    }
}

export default AddInformation;