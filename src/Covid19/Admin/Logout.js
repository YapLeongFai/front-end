import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './Admin.css';

export default class Logout extends Component{
    constructor(props){
        super(props)
        localStorage.removeItem("token")
    }
    render(){
        return(
            <div>
                <div  className="sideHeader"/>
                <div  style={{backgroundColor: "lightblue", padding:"2cm"}}>
                    <center>
                        <h1>You have been Logged out!!</h1>
                        <Link to="/">Home</Link>
                    </center>
                </div>
            </div>
        )
    }
}