import React, {Component} from 'react';
import {Link, Redirect} from 'react-router-dom';
import './Admin.css';

class DeleteInformation extends Component {
    constructor(props) {
        super(props)
        const token = localStorage.getItem("token")
        let loggedIn = true
        if (token == null) {
            loggedIn = false
        } else if (token.length < 10) {
            loggedIn = false
        }

        this.state = {
            loggedIn,
            keyword:''
        }
    }

    handleChange = event =>{
        this.setState({[event.target.name]:event.target.value})
    }

    handleSubmit = event=>{
        let Token="Bearer " + localStorage.getItem("token")
        console.log("keyword: " + this.state.keyword)
        let url = "/covid19/information/" + this.state.keyword
        fetch(url,{
            method: 'GET',
            headers:{
                'Authorization': Token,
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(response => {
                console.log(response)
                this.setState({information:response})
            })
            .catch(console.log)
    }

    render() {
        if (this.state.loggedIn === false) {
            return <Redirect to="/login"/>
        }

        return (
            <div>
                <div className="column side">
                    <div className="sideHeader"/>
                    <a><Link to="/admin/admin_information">Information</Link></a>
                    <a><Link to="/logout">Logout</Link></a>
                </div>
                <div className="column middle">
                    <h1 className="header">Admin</h1>
                    <div>
                        <form onSubmit={this.handleSubmit}>
                            <label name="keyword">Keyword:</label>
                            <input type="text" name="keyword" onChange={this.handleChange}/>
                            <input type="submit" value="Search"/>
                        </form>
                    </div>

                </div>
                <div className="column side">
                    <div className="sideHeader"/>
                    <a><Link to="/admin/admin_information/add">Add</Link></a>
                    <a><Link to="/admin/admin_information/delete">Delete</Link></a>
                </div>
            </div>

        )
    }
}

export default DeleteInformation;