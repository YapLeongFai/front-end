import React from 'react';
import { Link } from 'react-router-dom'; 
import './TopNav.css';

function TopNav(){
  return(
         <div className="TopNav">
          <ul>
            <li><Link to="/"><a>Home</a></Link></li>
            <li><Link to="/news"><a>News</a></Link></li>
            <li><Link to="/information"><a>Information</a></Link></li>
            <li><Link to="/login"><a>Admin</a></Link></li>
          </ul>
         </div>
       )
}
export default TopNav;