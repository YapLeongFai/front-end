import React from 'react';
import { Route, Link, Switch } from "react-router-dom";
import Home from './Covid19/Home/Home';
import News from './Covid19/News';
import Information from './Covid19/Information/Information';
import ADMIN_INFORMATION from './Covid19/Admin/AdminInformation';
import Login from './Covid19/Admin/Login';
import Logout from './Covid19/Admin/Logout';
import ADD_INFORMATION from './Covid19/Admin/AddInformation';
import DELETE_INFORMATION from './Covid19/Admin/DeleteInformation';

function App(){
  return(
    <Switch>
    <div className="App">
      <Route exact path="/" component={Home}/>
      <Route exact path="/news" component={News}/>
      <Route exact path="/information" component={Information}/>
      <Route exact path="/login" component={Login}/>
      <Route exact path="/admin/admin_information" component={ADMIN_INFORMATION}/>
      <Route path="/logout" component={Logout}/>
      <Route path="/admin/admin_information/add" component={ADD_INFORMATION}/>
      <Route path="/admin/admin_information/delete" component={DELETE_INFORMATION}/>
    </div>
    </Switch>
  )
}

export default App;